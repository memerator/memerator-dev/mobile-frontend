import 'package:flutter/material.dart';

class Header extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      color: Color(0xff131B2A),
      //color: Colors.red,
      child: Column(
        children: [
          Row(
            children: [
              Text(
                "FRINKIAC",
                style: TextStyle(
                  color: Color(0xffF7D629),
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(width: 180),
              Icon(Icons.access_alarm_rounded),
            ],
          ),
          Text("HOLAS"),
        ],
      ),
    );
  }
}
