import 'package:flutter/material.dart';
import 'package:meme/screen/widgets/header.dart';
import 'package:meme/screen/widgets/bodi.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xff1E2430),
      child: Column(children: [
        Header(),
        Bodi(),
      ]),
    );
  }
}
